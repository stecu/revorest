package com.stecalbert.revorest.dao;

import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.dao.spec.AccountDao;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.resource.ResourceException;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AccountDaoImplTest {
    private static AccountDao accountDao;

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        accountDao = new AccountDaoImpl();
    }

    @AfterAll
    static void tearDown() {
        ((AccountDaoImpl) accountDao).close();
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetAccountData();
    }

    @Test
    void getExistingAccountById() {
        Account account = accountDao.findById(1L);
        assertEquals("aventura", account.getUsername());
    }

    @Test
    void getExistingAccountByUsername() {
        Account account = accountDao.findByName("aventura");
        assertEquals(new Long(1), account.getId());
    }

    @Test
    void createValidAccount() {
        Account account = new Account("janko", new BigDecimal(3333), Currency.getInstance("PLN"));
        Account created = accountDao.create(account);
        assertEquals("janko", created.getUsername());
    }

    @Test
    void createInvalidAccount() {
        Account account = new Account(null, new BigDecimal(3333), Currency.getInstance("USD"));
        assertThrows(ResourceException.class, () -> accountDao.create(account));
    }

    @Test
    void deleteExistingUser() {
        Account account = accountDao.findByName("aventura");
        assertTrue(accountDao.delete(account));
    }

    @Test
    void findAllUsers() {
        List accounts = accountDao.findAll();
        assertEquals(4, accounts.size());
    }

}