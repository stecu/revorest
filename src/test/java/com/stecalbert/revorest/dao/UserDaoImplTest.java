package com.stecalbert.revorest.dao;

import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.dao.spec.UserDao;
import com.stecalbert.revorest.model.User;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.resource.ResourceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoImplTest {
    private static UserDao userDao;
    private static Connection conn;

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        userDao = new UserDaoImpl();
    }

    @AfterAll
    static void tearDown() {
        ((UserDaoImpl) userDao).close();
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetUserData();
    }

    @Test
    void getExistingUserById() {
        User user = userDao.findById(3L);
        assertEquals("foo", user.getUsername());
    }

    @Test
    void createValidUser() {
        User user = new User("janko", "Jan", "Wojtowiak");
        User created = userDao.create(user);
        assertEquals("janko", created.getUsername());
    }

    @Test
    void createInvalidUser() {
        User user = new User("janko", "Jan", null);
        assertThrows(ResourceException.class, () -> userDao.create(user));
    }

    @Test
    void deleteExistingUser() {
        assertTrue(userDao.delete(5L));
    }

    @Test
    void findAllUsers() {
        List users = userDao.findAll();
        assertEquals(5, users.size());
    }

    @Test
    void updateExistingUser() {
        User user = userDao.findById(1L);
        user.setName("Piotr");
        User updated = userDao.update(user);
        assertEquals("Piotr", updated.getName());
    }
}