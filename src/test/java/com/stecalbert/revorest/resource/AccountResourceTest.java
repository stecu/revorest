package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.util.JsonUtils;
import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AccountResourceTest {

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetAccountData();
    }

    @Test
    void getExistingAccount() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account/1");
        Representation response = clientResource.get(MediaType.APPLICATION_JSON);
        Account account = (Account) JsonUtils.getObjectFromJson(response.getText(), Account.class);
        assertEquals("aventura", account.getUsername());
    }

    @Test
    void getNonExistingAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account/10");
        assertThrows(ResourceException.class,
                clientResource::get);
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());
    }

    @Test
    void deleteExistingAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account/2");
        clientResource.delete();
        assertEquals(Status.SUCCESS_NO_CONTENT, clientResource.getStatus());
    }

    @Test
    void deleteNoneExistingAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account/10");
        assertThrows(ResourceException.class,
                clientResource::delete);
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());
    }

    @Test
    void createValidAccount() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account");
        String json = "{\"username\":\"mario_33\",\"balance\":5001,\"currency\":\"USD\"}";
        Representation response = clientResource.post(json, MediaType.APPLICATION_JSON);
        Account account = (Account) JsonUtils.getObjectFromJson(response.getText(), Account.class);
        assertEquals("mario_33", account.getUsername());
    }

    @Test
    void createInvalidAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/account");
        String json = "{\"username\":\"mario_33\",\"balance\":5001}";
        assertThrows(ResourceException.class, () -> clientResource.post(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.SERVER_ERROR_INTERNAL, clientResource.getStatus());
    }

}