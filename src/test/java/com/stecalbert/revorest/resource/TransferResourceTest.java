package com.stecalbert.revorest.resource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverter;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverterInjector;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferResourceTest {
    private static CurrencyConverter currencyConverter;

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();

        currencyConverter = mock(CurrencyConverter.class);
        when(currencyConverter.convert(any(), any(), any()))
                .thenAnswer(invocationOnMock -> {
                    BigDecimal amount = (BigDecimal) invocationOnMock.getArguments()[2];
                    return amount.multiply(new BigDecimal(2));
                });
        CurrencyConverterInjector.inject(currencyConverter);
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetAccountData();
    }

    @Test
    void sameCurrencyCorrectTransfer() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":3, \"amount\":50}";
        Representation response = clientResource.put(json, MediaType.APPLICATION_JSON);
        List<Account> accounts =
                new ObjectMapper().readValue(response.getText(), new TypeReference<List<Account>>() {
                });
        assertEquals(new BigDecimal(50.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(0).getBalance().setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(new BigDecimal(750.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(1).getBalance().setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    void differentCurrencyCorrectTransfer() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":2, \"amount\":50}";
        Representation response = clientResource.put(json, MediaType.APPLICATION_JSON);
        List<Account> accounts =
                new ObjectMapper().readValue(response.getText(), new TypeReference<List<Account>>() {
                });
        assertEquals(new BigDecimal(50.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(0).getBalance().setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(new BigDecimal(10100.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(1).getBalance().setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    void transferAllFundsFromAccount() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":3, \"amount\":100}";
        Representation response = clientResource.put(json, MediaType.APPLICATION_JSON);
        List<Account> accounts =
                new ObjectMapper().readValue(response.getText(), new TypeReference<List<Account>>() {
                });
        assertEquals(new BigDecimal(0.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(0).getBalance().setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(new BigDecimal(800.00).setScale(2, RoundingMode.HALF_EVEN),
                accounts.get(1).getBalance().setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    void transferNonExistingAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":10, \"amount\":50}";
        assertThrows(ResourceException.class, () -> clientResource.put(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.CLIENT_ERROR_BAD_REQUEST, clientResource.getStatus());
    }

    @Test
    void transferZeroAmount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":2, \"amount\":0}";
        assertThrows(ResourceException.class, () -> clientResource.put(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.CLIENT_ERROR_BAD_REQUEST, clientResource.getStatus());
    }

    @Test
    void transferAmountGreaterThanBalance() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/transfer");
        String json = "{\"idFrom\": 1, \"idTo\":3, \"amount\":200}";
        assertThrows(ResourceException.class, () -> clientResource.put(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.CLIENT_ERROR_BAD_REQUEST, clientResource.getStatus());
    }

}