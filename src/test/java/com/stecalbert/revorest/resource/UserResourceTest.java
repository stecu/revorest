package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.model.User;
import com.stecalbert.revorest.util.JsonUtils;
import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserResourceTest {
    private static Connection conn;

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetUserData();
    }

    @Test
    void getExistingUser() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/1");
        Representation response = clientResource.get(MediaType.APPLICATION_JSON);
        User user = (User) JsonUtils.getObjectFromJson(response.getText(), User.class);
        assertEquals("mario_33", user.getUsername());
    }

    @Test
    void getNonExistingUser() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/10");
        assertThrows(ResourceException.class,
                clientResource::delete);
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());
    }

    @Test
    void deleteExistingUser() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/1");
        clientResource.delete();
        assertEquals(Status.SUCCESS_NO_CONTENT, clientResource.getStatus());
    }

    @Test
    void deleteNoneExistingUser() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/10");

        assertThrows(ResourceException.class,
                clientResource::delete);
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());

    }

    @Test
    void createValidUser() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user");
        String json = "{\n" +
                "\t\"name\": \"Marian\",\n" +
                "\t\"surname\": \"Kruk\",\n" +
                "\t\"username\": \"marian_92\"\n" +
                "}";
        Representation response = clientResource.post(json, MediaType.APPLICATION_JSON);
        User user = (User) JsonUtils.getObjectFromJson(response.getText(), User.class);
        assertEquals("marian_92", user.getUsername());
    }

    @Test
    void createInvalidUser() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user");
        String json = "{\n" +
                "\t\"name\": \"Marian\",\n" +
                "\t\"surname\": \"Kruk\"\n" +
                "}";
        assertThrows(ResourceException.class, () -> clientResource.post(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.SERVER_ERROR_INTERNAL, clientResource.getStatus());
    }

    @Test
    void updateUser() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/1");
        String json = "{\n" +
                "\t\"name\": \"Marek\",\n" +
                "\t\"surname\": \"Kowalczyk\",\n" +
                "\t\"username\": \"mario_33\"\n" +
                "}";
        Representation response = clientResource.put(json, MediaType.APPLICATION_JSON);
        User user = (User) JsonUtils.getObjectFromJson(response.getText(), User.class);
        assertEquals("mario_33", user.getUsername());
        assertEquals("Kowalczyk", user.getSurname());
    }

    @Test
    void updateNonExistingUser() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/user/10");
        String json = "{\n" +
                "\t\"name\": \"Marek\",\n" +
                "\t\"surname\": \"Kowalczyk\",\n" +
                "\t\"username\": \"mario_33\"\n" +
                "}";
        assertThrows(ResourceException.class, () -> {
            clientResource.put(json, MediaType.APPLICATION_JSON);
        });
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());
    }
}