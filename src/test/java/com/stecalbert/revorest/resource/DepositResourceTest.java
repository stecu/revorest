package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.util.JsonUtils;
import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DepositResourceTest {
    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();
    }

    @BeforeEach
    void resetData() throws SQLException {
        TestUtils.resetAccountData();
    }

    @Test
    void depositValidAmount() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/deposit");
        String json = "{\"accountId\":\"1\",\"amount\":200.10}";
        Representation response = clientResource.put(json, MediaType.APPLICATION_JSON);
        Account account = (Account) JsonUtils.getObjectFromJson(response.getText(), Account.class);
        BigDecimal expected = new BigDecimal(300.10).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        BigDecimal actual = account.getBalance().setScale(2, BigDecimal.ROUND_HALF_DOWN);
        assertEquals(expected, actual);
    }

    @Test
    void depositInvalidAmount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/deposit");
        String json = "{\"accountId\":\"1\",\"amount\":-10}";
        assertThrows(ResourceException.class, () -> clientResource.put(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.CLIENT_ERROR_BAD_REQUEST, clientResource.getStatus());
    }

    @Test
    void withdrawFromNonExistingAccount() {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/deposit");
        String json = "{\"accountId\":\"8\",\"amount\": 100}";
        assertThrows(ResourceException.class, () -> clientResource.put(json, MediaType.APPLICATION_JSON));
        assertEquals(Status.CLIENT_ERROR_NOT_FOUND, clientResource.getStatus());
    }
}