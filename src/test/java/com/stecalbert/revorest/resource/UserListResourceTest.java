package com.stecalbert.revorest.resource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stecalbert.revorest.TestUtils;
import com.stecalbert.revorest.model.User;
import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserListResourceTest {
    private static Connection conn;

    @BeforeAll
    static void arrange() throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();
    }

    @BeforeEach
    void resetData() throws SQLException {

        TestUtils.resetUserData();
    }

    @Test
    void getUsers() throws IOException {
        ClientResource clientResource = new ClientResource("http://localhost:"
                + ServerUtils.PORT + "/users");
        Representation response = clientResource.get(MediaType.APPLICATION_JSON);
        List<User> users = new ObjectMapper().readValue(response.getText(), new TypeReference<List<User>>() {
        });
        assertEquals(5, users.size());
    }

}