package com.stecalbert.revorest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestUtils {
    public static void resetUserData() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:test");
        conn.createStatement().execute("DELETE FROM USER;");
        conn.createStatement().execute("INSERT INTO USER(id, VERSION, USERNAME, FIRST_NAME, SURNAME) VALUES (1, 0, 'mario_33', 'Marek', 'Kowalski');");
        conn.createStatement().execute("INSERT INTO USER(id, VERSION, USERNAME, FIRST_NAME, SURNAME) VALUES (2, 0,'aventura', 'Jan', 'Kowalski');");
        conn.createStatement().execute("INSERT INTO USER(id, VERSION, USERNAME, FIRST_NAME, SURNAME) VALUES (3, 0,'foo', 'Kazimierz', 'Kania');");
        conn.createStatement().execute("INSERT INTO USER(id, VERSION, USERNAME, FIRST_NAME, SURNAME) VALUES (4, 0,'bar', 'Bartek', 'Panek');");
        conn.createStatement().execute("INSERT INTO USER(id, VERSION, USERNAME, FIRST_NAME, SURNAME) VALUES (5, 0,'mario', 'Marek', 'Kowalski');");
    }

    public static void resetAccountData() throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:test");
        conn.createStatement().execute("DELETE FROM ACCOUNT;");
        conn.createStatement().execute("INSERT INTO ACCOUNT(id, VERSION, USERNAME, BALANCE, CURRENCY) VALUES (1, 0, 'aventura', 100, 'USD');");
        conn.createStatement().execute("INSERT INTO ACCOUNT(id, VERSION, USERNAME, BALANCE, CURRENCY) VALUES (2, 0, 'foo', 10000, 'EUR');");
        conn.createStatement().execute("INSERT INTO ACCOUNT(id, VERSION, USERNAME, BALANCE, CURRENCY) VALUES (3, 0, 'bar', 700, 'USD');");
        conn.createStatement().execute("INSERT INTO ACCOUNT(id, VERSION, USERNAME, BALANCE, CURRENCY) VALUES (4, 0, 'ipsum', 0, 'PLN');");
    }


}
