package com.stecalbert.revorest;

import com.stecalbert.revorest.util.ServerUtils;
import com.stecalbert.revorest.util.SqlUtils;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverterInjector;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverterImpl;

public class Main {

    public static void main(String[] args) throws Exception {
        SqlUtils.createTables();
        ServerUtils.createRestletServer();
        CurrencyConverterInjector.inject(new CurrencyConverterImpl());
    }
}
