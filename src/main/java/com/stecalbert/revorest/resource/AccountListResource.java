package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.service.AccountService;
import com.stecalbert.revorest.util.JsonUtils;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class AccountListResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(AccountListResource.class.getName());
    private AccountService accountService = new AccountService();

    @Get
    public Representation getAccounts() {
        StringRepresentation response = null;
        LOGGER.log(Level.INFO, "/users GET invoked ");
        List accounts = accountService.getAllAccounts();
        return JsonUtils.getListStringRepresentation(accounts);
    }
}