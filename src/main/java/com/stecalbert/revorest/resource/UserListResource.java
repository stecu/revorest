package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.service.UserService;
import com.stecalbert.revorest.util.JsonUtils;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserListResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(UserListResource.class.getName());
    private UserService userService = new UserService();

    @Get
    public Representation getUsers() {
        StringRepresentation response = null;
        LOGGER.log(Level.INFO, "/users GET invoked ");
        List users = userService.getAllUsers();
        return JsonUtils.getListStringRepresentation(users);
    }
}