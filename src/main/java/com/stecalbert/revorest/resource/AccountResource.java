package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.service.AccountService;
import com.stecalbert.revorest.util.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(AccountResource.class.getName());
    private AccountService accountService = new AccountService();


    @Get
    public Representation getAccount() {
        StringRepresentation response = null;
        String parameter = (String) getRequest().getAttributes().get("id");
        LOGGER.log(Level.INFO, "/account GET invoked with parameter {0}", parameter);
        if (NumberUtils.isDigits(parameter)) {
            Long id = Long.valueOf(parameter);
            Account account = accountService.getAccountById(id);
            response = JsonUtils.getObjectStringRepresentation(account);
        } else if (StringUtils.isNotBlank(parameter)) {
            Account account = accountService.getAccountByName(parameter);
            response = JsonUtils.getObjectStringRepresentation(account);
        }
        return response;
    }


    @Post
    public Representation createAccount(Representation entity) {
        LOGGER.log(Level.INFO, "/account POST invoked with data {0}", entity != null ? entity.toString() : null);
        String json = JsonUtils.getEntityAsString(entity);
        Account account = (Account) JsonUtils.getObjectFromJson(json, Account.class);
        Account created = accountService.createAccount(account);
        String response = created != null ? JsonUtils.getObjectAsJson(created) : StringUtils.EMPTY;
        return new StringRepresentation(response);
    }

    @Delete
    public Representation deleteAccount() {
        String accountId = (String) getRequest().getAttributes().get("id");
        LOGGER.log(Level.INFO, "/account DELETE invoked with parameter {0}", accountId);
        if (NumberUtils.isDigits(accountId)) {
            Long id = Long.valueOf(accountId);
            Status status = accountService.deleteAccount(id) ? Status.SUCCESS_NO_CONTENT : Status.CLIENT_ERROR_NOT_FOUND;
            getResponse().setStatus(status);
        }
        return new StringRepresentation(StringUtils.EMPTY);
    }

}