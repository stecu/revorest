package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.model.BalanceOperation;
import com.stecalbert.revorest.service.AccountService;
import com.stecalbert.revorest.util.JsonUtils;
import org.restlet.representation.Representation;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import java.util.logging.Level;
import java.util.logging.Logger;

public class WithdrawResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(WithdrawResource.class.getName());
    private AccountService accountService = new AccountService();

    @Put
    public Representation withdraw(Representation entity) {
        LOGGER.log(Level.INFO, "/withdraw PUT invoked with data {0}", entity != null ? entity.toString() : null);
        String json = JsonUtils.getEntityAsString(entity);
        BalanceOperation balanceOperation = (BalanceOperation) JsonUtils.getObjectFromJson(json, BalanceOperation.class);
        Account updated = accountService.withdraw(balanceOperation);
        return JsonUtils.getObjectStringRepresentation(updated);
    }

}