package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.model.User;
import com.stecalbert.revorest.service.UserService;
import com.stecalbert.revorest.util.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UserResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(UserResource.class.getName());
    private UserService userService = new UserService();


    @Get
    public Representation getUser() {
        StringRepresentation response = null;
        String userId = (String) getRequest().getAttributes().get("id");
        LOGGER.log(Level.INFO, "/user GET invoked with parameter {0}", userId);
        if (NumberUtils.isDigits(userId)) {
            Long id = Long.valueOf(userId);
            User user = userService.getUser(id);
            response = JsonUtils.getObjectStringRepresentation(user);
        }
        return response;
    }

    @Post
    public Representation createUser(Representation entity) {
        LOGGER.log(Level.INFO, "/user POST invoked with data {0}", entity != null ? entity.toString() : null);
        String json = JsonUtils.getEntityAsString(entity);
        User user = (User) JsonUtils.getObjectFromJson(json, User.class);
        User created = userService.createUser(user);
        String response = created != null ? JsonUtils.getObjectAsJson(created) : StringUtils.EMPTY;
        return new StringRepresentation(response);
    }

    @Delete
    public Representation deleteUser() {
        String userId = (String) getRequest().getAttributes().get("id");
        LOGGER.log(Level.INFO, "/user DELETE invoked with parameter {0}", userId);
        if (NumberUtils.isDigits(userId)) {
            Long id = Long.valueOf(userId);
            Status status = userService.deleteUser(id) ? Status.SUCCESS_NO_CONTENT : Status.CLIENT_ERROR_NOT_FOUND;
            getResponse().setStatus(status);
        }
        return new StringRepresentation(StringUtils.EMPTY);
    }

    @Put
    public Representation updateUser(Representation entity) {
        StringRepresentation response = null;
        String userId = (String) getRequest().getAttributes().get("id");
        LOGGER.log(Level.INFO, "{0},{1},{2},{3}",
                new Object[]{"/user UPDATE invoked with data:", entity != null ? entity.toString() : null, " and id: ", userId});
        if (NumberUtils.isDigits(userId)) {
            Long id = Long.valueOf(userId);
            String json = JsonUtils.getEntityAsString(entity);
            User user = (User) JsonUtils.getObjectFromJson(json, User.class);
            User updated = userService.updateUser(id, user);
            response = JsonUtils.getObjectStringRepresentation(updated);
        }
        return response;
    }


}