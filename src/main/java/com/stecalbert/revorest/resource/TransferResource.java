package com.stecalbert.revorest.resource;

import com.stecalbert.revorest.model.Transfer;
import com.stecalbert.revorest.service.TransferService;
import com.stecalbert.revorest.util.JsonUtils;
import org.restlet.representation.Representation;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransferResource extends ServerResource {
    private static final Logger LOGGER = Logger.getLogger(TransferResource.class.getName());
    private TransferService transferService = new TransferService();

    @Put
    public Representation Representation(Representation entity) {
        LOGGER.log(Level.INFO, "/transfer PUT invoked with data {0}", entity != null ? entity.toString() : null);
        String json = JsonUtils.getEntityAsString(entity);
        Transfer transfer = (Transfer) JsonUtils.getObjectFromJson(json, Transfer.class);
        List result = transferService.transfer(transfer);
        return JsonUtils.getListStringRepresentation(result);
    }

}