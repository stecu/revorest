package com.stecalbert.revorest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @JsonIgnore
    @Setter(AccessLevel.NONE)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id;

    @Version
    @Setter(AccessLevel.NONE)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonIgnore
    private Long version;

    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "BALANCE", nullable = false)
    private BigDecimal balance;

    @Column(name = "CURRENCY", nullable = false)
    private Currency currency;

    @JsonCreator
    public Account(@JsonProperty("username") String username,
                   @JsonProperty("balance") BigDecimal balance,
                   @JsonProperty("currency") Currency currency) {
        this.username = username;
        this.balance = balance;
        this.currency = currency;
    }

    public Account() {
    }
}
