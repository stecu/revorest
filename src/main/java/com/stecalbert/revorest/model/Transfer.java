package com.stecalbert.revorest.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Transfer {
    private Long idFrom;
    private Long idTo;
    private BigDecimal amount;
}
