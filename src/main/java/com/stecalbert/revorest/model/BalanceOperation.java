package com.stecalbert.revorest.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BalanceOperation {
    Long accountId;
    BigDecimal amount;
}
