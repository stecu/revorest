package com.stecalbert.revorest.dao;

import com.stecalbert.revorest.dao.spec.UserDao;
import com.stecalbert.revorest.model.User;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDaoImpl implements UserDao {
    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class.getName());

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("persUnit");
    private EntityManager em;

    public UserDaoImpl() {
        em = emf.createEntityManager();
    }

    @Override
    public User create(User user) {
        em.getTransaction().begin();
        try {
            em.persist(user);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "User {0} created", user);
            return user;
        } catch (Exception e) {
            String msg = "An exception was thrown while adding user to database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public User findById(Long id) {
        em.getTransaction().begin();
        try {
            User user = em.find(User.class, id);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "User {0} findById", user);
            return user;
        } catch (Exception e) {
            String msg = "An exception was thrown while getting user from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public List findAll() {
        em.getTransaction().begin();
        try {
            Query queryResult = em.createQuery("from User");
            java.util.List allUsers;
            allUsers = queryResult.getResultList();
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Fetched {0} users", allUsers.size());
            return allUsers;
        } catch (Exception e) {
            String msg = "An exception was thrown while getting users from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public User update(User user) {
        em.getTransaction().begin();
        try {
            user = em.merge(user);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "User {0} updated :", user);
            return user;
        } catch (Exception e) {
            String msg = "An exception was thrown while merging user to database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public boolean delete(Long id) {
        em.getTransaction().begin();
        try {
            User user = em.find(User.class, id);
            if (user != null) {
                em.remove(user);
            } else {
                return false;
            }
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "User {0} deleted:", user);
            return true;
        } catch (Exception e) {
            String msg = "An exception was thrown while deleting user from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    void close() {
        em.clear();
        em.close();
        emf.close();
    }
}
