package com.stecalbert.revorest.dao;

import com.stecalbert.revorest.dao.spec.AccountDao;
import com.stecalbert.revorest.model.Account;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountDaoImpl implements AccountDao {
    private static final Logger LOGGER = Logger.getLogger(AccountDaoImpl.class.getName());

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("persUnit");
    private EntityManager em;

    public AccountDaoImpl() {
        em = emf.createEntityManager();
    }

    public Account create(Account account) {
        em.getTransaction().begin();
        try {
            em.persist(account);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Account {0} created", account);
            return account;
        } catch (Exception e) {
            String msg = "An exception was thrown while adding account to database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    public Account findById(Long id) {
        em.getTransaction().begin();
        try {
            Account account = em.find(Account.class, id);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Account {0} findById", account);
            return account;
        } catch (Exception e) {
            String msg = "An exception was thrown while getting account from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public Account findByName(String name) {
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("from Account WHERE username=?1");
            query.setParameter(1, name);
            Account account = (Account) query.getSingleResult();
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Account {0} findByName", account);
            return account;
        } catch (Exception e) {
            String msg = "An exception was thrown while getting account from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    @Override
    public Account update(Account account) {
        em.getTransaction().begin();
        try {
            account = em.merge(account);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Account {0} updated :", account);
            return account;
        } catch (Exception e) {
            String msg = "An exception was thrown while merging account to database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    public List findAll() {
        em.getTransaction().begin();
        try {
            Query queryResult = em.createQuery("from Account");
            List allAccounts;
            allAccounts = queryResult.getResultList();
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Fetched {0} accounts", allAccounts.size());
            return allAccounts;
        } catch (Exception e) {
            String msg = "An exception was thrown while getting accounts from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    public boolean delete(Account account) {
        em.getTransaction().begin();
        try {
            em.remove(account);
            em.getTransaction().commit();
            LOGGER.log(Level.INFO, "Account {0} deleted:", account);
            return true;
        } catch (Exception e) {
            String msg = "An exception was thrown while deleting account from database";
            LOGGER.log(Level.SEVERE, msg, e);
            em.getTransaction().rollback();
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, msg);
        }
    }

    void close() {
        em.clear();
        em.close();
        emf.close();
    }
}
