package com.stecalbert.revorest.dao.spec;

import com.stecalbert.revorest.model.Account;

import java.util.List;

public interface AccountDao {
    Account create(Account user);

    Account findById(Long id);

    Account findByName(String name);

    Account update(Account account);

    List findAll();

    boolean delete(Account account);

}
