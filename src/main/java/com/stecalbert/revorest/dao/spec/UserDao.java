package com.stecalbert.revorest.dao.spec;

import com.stecalbert.revorest.model.User;

import java.util.List;

public interface UserDao {
    User create(User user);

    User findById(Long id);

    List findAll();

    User update(User user);

    boolean delete(Long id);

}
