package com.stecalbert.revorest.service;

import com.stecalbert.revorest.dao.AccountDaoImpl;
import com.stecalbert.revorest.dao.spec.AccountDao;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.model.BalanceOperation;
import com.stecalbert.revorest.util.TernaryExtension;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountService {
    private static final Logger LOGGER = Logger.getLogger(AccountService.class.getName());
    private AccountDao accountDao;

    public AccountService() {
        accountDao = new AccountDaoImpl();
    }

    public Account createAccount(Account account) {
        return accountDao.create(account);
    }

    public Account getAccountById(Long id) {
        Account account = accountDao.findById(id);
        validateFetchedAccount(account);
        return account;
    }

    public Account getAccountByName(String name) {
        return accountDao.findByName(name);
    }

    public boolean deleteAccount(Long id) {
        Account account = accountDao.findById(id);
        validateFetchedAccount(account);
        return accountDao.delete(account);
    }

    public List getAllAccounts() {
        return accountDao.findAll();
    }

    public Account deposit(BalanceOperation balanceOperation) {
        Account updatedAccount;
        Account accountToUpdate = accountDao.findById(balanceOperation.getAccountId());
        validateFetchedAccount(accountToUpdate);

        BigDecimal balance = accountToUpdate.getBalance();
        BigDecimal amount = balanceOperation.getAmount().signum() > 0
                ? balanceOperation.getAmount() : TernaryExtension.throwResourceException("Invalid amount");
        accountToUpdate.setBalance(balance.add(amount));
        updatedAccount = accountDao.update(accountToUpdate);

        return updatedAccount;
    }

    public Account withdraw(BalanceOperation balanceOperation) {
        Account updatedAccount;
        Account accountToUpdate = accountDao.findById(balanceOperation.getAccountId());
        validateFetchedAccount(accountToUpdate);

        BigDecimal balance = accountToUpdate.getBalance().signum() > 0
                ? accountToUpdate.getBalance() : TernaryExtension.throwResourceException("Insufficient balance");
        BigDecimal amount = balanceOperation.getAmount().signum() > 0
                ? balanceOperation.getAmount() : TernaryExtension.throwResourceException("Invalid amount");
        BigDecimal newBalance = balance.subtract(amount);
        accountToUpdate.setBalance(newBalance);
        updatedAccount = accountDao.update(accountToUpdate);

        return updatedAccount;
    }

    private void validateFetchedAccount(Account account) {
        if (account == null) {
            LOGGER.log(Level.INFO, "Couldn't find account of id");
            throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, "Couldn't find account of given id");
        }
    }
}

