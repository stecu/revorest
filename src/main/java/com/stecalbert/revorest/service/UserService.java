package com.stecalbert.revorest.service;

import com.stecalbert.revorest.dao.UserDaoImpl;
import com.stecalbert.revorest.dao.spec.UserDao;
import com.stecalbert.revorest.model.User;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import java.util.List;

public class UserService {
    private UserDao userDao;

    public UserService() {
        userDao = new UserDaoImpl();
    }

    public User createUser(User u) {
        return userDao.create(u);
    }

    public User getUser(Long id) {
        User user = userDao.findById(id);
        validateFetchedUser(user);
        return user;
    }

    public boolean deleteUser(Long id) {
        return userDao.delete(id);
    }

    public User updateUser(Long id, User modifiedUser) {
        User userToUpdate = userDao.findById(id);
        validateFetchedUser(userToUpdate);
        userToUpdate.setUsername(modifiedUser.getUsername());
        userToUpdate.setSurname(modifiedUser.getSurname());
        userToUpdate.setName(modifiedUser.getName());
        return userDao.update(userToUpdate);
    }

    public List getAllUsers() {
        return userDao.findAll();
    }

    private void validateFetchedUser(User user) {
        if (user == null) {
            throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, "Couldn't find user of given id");
        }
    }
}
