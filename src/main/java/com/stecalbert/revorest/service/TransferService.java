package com.stecalbert.revorest.service;

import com.stecalbert.revorest.dao.AccountDaoImpl;
import com.stecalbert.revorest.dao.spec.AccountDao;
import com.stecalbert.revorest.model.Account;
import com.stecalbert.revorest.model.Transfer;
import com.stecalbert.revorest.util.TernaryExtension;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverter;
import com.stecalbert.revorest.util.currency_conversion.CurrencyConverterInjector;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

public class TransferService {
    private CurrencyConverter currencyConverter = CurrencyConverterInjector.getInstance();
    private AccountDao accountDao;

    public TransferService() {
        accountDao = new AccountDaoImpl();
    }

    public List<Account> transfer(Transfer transfer) {
        validateTransferAmount(transfer.getAmount());
        Account accountFrom = accountDao.findById(transfer.getIdFrom());
        Account accountTo = accountDao.findById(transfer.getIdTo());
        validateAccounts(accountFrom, accountTo);

        BigDecimal amountInDestinationCurrency = getAmountInDestinationAccountCurrency(
                accountFrom.getCurrency(), accountTo.getCurrency(), transfer.getAmount());
        BigDecimal accountFromBalanceAfterTransfer =
                getAccountFromBalanceAfterTransfer(accountFrom, transfer.getAmount());
        BigDecimal accountToBalanceAfterTransfer =
                accountTo.getBalance().add(amountInDestinationCurrency);
        accountFrom.setBalance(accountFromBalanceAfterTransfer
                .setScale(accountFrom.getCurrency().getDefaultFractionDigits(), RoundingMode.HALF_EVEN));
        accountTo.setBalance(accountToBalanceAfterTransfer
                .setScale(accountTo.getCurrency().getDefaultFractionDigits(), RoundingMode.HALF_EVEN));
        Account updatedFromAccount = accountDao.update(accountFrom);
        Account updatedToAccount = accountDao.update(accountTo);

        return Arrays.asList(updatedFromAccount, updatedToAccount);
    }

    private void validateTransferAmount(BigDecimal amount) {
        if (amount.signum() <= 0) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Invalid amount");
        }
    }

    private BigDecimal getAmountInDestinationAccountCurrency(Currency from, Currency to, BigDecimal amount) {
        boolean isSameCurrency = from.getCurrencyCode().equals(to.getCurrencyCode());
        return isSameCurrency
                ? amount
                : currencyConverter.convert(from, to, amount);
    }

    private BigDecimal getAccountFromBalanceAfterTransfer(Account accountFrom, BigDecimal amount) {
        BigDecimal accountFromBalance = accountFrom.getBalance();
        BigDecimal accountFromSubstractedByAmount = accountFromBalance.subtract(amount);
        boolean hasSufficientFunds = accountFromSubstractedByAmount.doubleValue() >= 0;
        return hasSufficientFunds
                ? accountFromSubstractedByAmount
                : TernaryExtension.throwResourceException("Insufficient balance");
    }

    private void validateAccounts(Account accountFrom, Account accountTo) {
        if (accountFrom == null || accountTo == null) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST,
                    "Cannot transfer from or to non existing account");
        }
    }
}
