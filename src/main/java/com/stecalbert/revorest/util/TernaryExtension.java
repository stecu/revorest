package com.stecalbert.revorest.util;

import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TernaryExtension {
    private static final Logger LOGGER = Logger.getLogger(TernaryExtension.class.getName());

    public static <T> T throwResourceException(String msg) {
        LOGGER.log(Level.SEVERE, msg);
        throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, msg);
    }

}
