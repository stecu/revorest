package com.stecalbert.revorest.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlUtils {
    public static void createTables() throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:test");
        conn.createStatement().execute("create table USER(id IDENTITY PRIMARY KEY, USERNAME varchar(255), VERSION BIGINT(20),FIRST_NAME varchar(255), SURNAME varchar(255));");
        conn.createStatement().execute("create table ACCOUNT(id IDENTITY PRIMARY KEY, VERSION BIGINT(20) ,USERNAME varchar(255), BALANCE DECIMAL , CURRENCY varchar (255));");
    }
}
