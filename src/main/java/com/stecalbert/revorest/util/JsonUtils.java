package com.stecalbert.revorest.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonUtils {
    private static final Logger LOGGER = Logger.getLogger(JsonUtils.class.getName());

    public static String getEntityAsString(Representation entity) {
        try {
            return entity.getText();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An exception was thrown: Cannot parse request entity", e);
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Cannot parse request entity");
        }
    }

    public static Object getObjectFromJson(String json, Class type) {
        try {
            return new ObjectMapper().readValue(json, type);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An exception was thrown: Cannot convert entity to User object", e);
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Cannot convert entity to User object");
        }
    }

    public static String getObjectAsJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL);
        }
    }

    public static StringRepresentation getObjectStringRepresentation(Object object) {
        StringRepresentation responseRespresentation;
            String createdJson;
            try {
                createdJson = new ObjectMapper().writeValueAsString(object);
                responseRespresentation = new StringRepresentation(createdJson);
            } catch (JsonProcessingException e) {
                LOGGER.log(Level.SEVERE, "An exception was thrown: Cannot convert response object to StringRepresentation", e);
                throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "Cannot convert response object to StringRepresentation");
            }
        return responseRespresentation;
    }

    public static StringRepresentation getListStringRepresentation(List<Object> objects) {
        try {
            String json = new ObjectMapper().writeValueAsString(objects);
            return new StringRepresentation(json);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, "An error occured while converting list of users to json", e);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "An error occured while converting list of users to json");
        }
    }
}
