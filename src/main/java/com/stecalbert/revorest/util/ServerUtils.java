package com.stecalbert.revorest.util;

import com.stecalbert.revorest.resource.*;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.routing.Template;

public class ServerUtils {
    public static final int PORT = 8182;

    public static void createRestletServer() throws Exception {
        final Component component = new Component();
        Server server = component.getServers().add(Protocol.HTTP, PORT);
        server.getContext().getParameters().add("useForwardedForHeader", "true");
        component.getInternalRouter().setDefaultMatchingMode(Template.MODE_EQUALS);
        component.getDefaultHost().attach("/user", UserResource.class);
        component.getDefaultHost().attach("/user/{id}", UserResource.class);
        component.getDefaultHost().attach("/users", UserListResource.class);
        component.getDefaultHost().attach("/account", AccountResource.class);
        component.getDefaultHost().attach("/account/{id}", AccountResource.class);
        component.getDefaultHost().attach("/accounts", AccountListResource.class);
        component.getDefaultHost().attach("/deposit", DepositResource.class);
        component.getDefaultHost().attach("/withdraw", WithdrawResource.class);
        component.getDefaultHost().attach("/transfer", TransferResource.class);
        component.start();
    }
}
