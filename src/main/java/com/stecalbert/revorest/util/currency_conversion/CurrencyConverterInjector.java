package com.stecalbert.revorest.util.currency_conversion;

public class CurrencyConverterInjector {
    private static CurrencyConverter instance = null;

    private CurrencyConverterInjector() {
    }

    public static CurrencyConverter getInstance() {
        return instance;
    }

    public static void inject(CurrencyConverter currencyConverter) {
        if (instance == null) {
            instance = currencyConverter;
        }
    }
}