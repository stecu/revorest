package com.stecalbert.revorest.util.currency_conversion;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stecalbert.revorest.util.TernaryExtension;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Protocol;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurrencyConverterImpl implements CurrencyConverter {
    private static final Logger LOGGER = Logger.getLogger(CurrencyConverterImpl.class.getName());

    private static BigDecimal getRateFromApi(Currency from, Currency to) {
        String from_to = from.getCurrencyCode() + "_" + to.getCurrencyCode();
        Client client = new Client(new Context(), Protocol.HTTP);
        ClientResource clientResource =
                new ClientResource("http://free.currencyconverterapi.com/api/v5/convert?compact=y&q=" + from_to);
        clientResource.setNext(client);
        Representation response = clientResource.get(MediaType.APPLICATION_JSON);
        return getRateFromJson(response, from_to);
    }

    private static BigDecimal getRateFromJson(Representation response, String from_to) {
        BigDecimal rate = null;
        try {
            String json = response.getText();
            JsonNode productNode = new ObjectMapper().readTree(json);
            String value = productNode.get(from_to).get("val").toString();
            rate = new BigDecimal(value);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An error with currency data provider occured", e);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "An error with currency data provider occured");
        }

        return rate;
    }

    @Override
    public BigDecimal convert(Currency from, Currency to, BigDecimal amount) {
        BigDecimal rate = getRateFromApi(from, to);
        BigDecimal convertedValue = rate.signum() > 0
                ? amount.multiply(rate)
                : TernaryExtension.throwResourceException(
                "An error occured while converting currencies. Invalid rate.");
        return convertedValue.setScale(to.getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }
}
