package com.stecalbert.revorest.util.currency_conversion;

import java.math.BigDecimal;
import java.util.Currency;

public interface CurrencyConverter {
    BigDecimal convert(Currency from, Currency to, BigDecimal amount);
}
