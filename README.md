# 1. Description

Money transfer API created for recrutation purposes.

# 2. Run application

Download project and go to _/out_ directory then execute in cmd:

```bash
java -jar revorest.jar
```

# 3. Build application
```bash
mvn clean install
```

# 4. Documentation

https://documenter.getpostman.com/view/6043197/RzfmERza?fbclid=IwAR1tqD98DuteEP_CfOuPXTgy0v58s-_AvWVWNCaGKyFZwWOm1WAjyd8vlZ8